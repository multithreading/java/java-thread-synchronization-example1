package com.honor;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
	    Message message = new Message();
        (new Thread(new MessageWriter(message))).start();
        (new Thread(new MessageReader(message))).start();
    }
}

class Message{

    private String message;
    private Boolean empty = true;

    public synchronized String read(){
        while(empty){
            try{
                /*
                * without wait, notify and notifyAll methods, we would have a deadlock situation here.
                * */
                wait();
            }catch(InterruptedException ex){

            }
        }
        empty = true;
        notifyAll();
        return message;
    }

    public synchronized void write(String message){
        while(!empty){
            try{
                wait();
            }catch(InterruptedException ex){

            }
        }
        this.message = message;
        empty = false;
        notifyAll();
    }
}

class MessageWriter implements Runnable{
    private Message message;

    public MessageWriter(Message message){
        this.message = message;
    }

    public void run(){
        String messages[] = {"message1", "message2", "message3", "message4"};
        Random random = new Random();
        for (int i=1;i<messages.length;i++){
            message.write(messages[i]);
            try{
                Thread.sleep(random.nextInt(3000));
            }catch(InterruptedException ex){

            }
            message.write("finished");
        }
    }
}

class MessageReader implements Runnable{

    private Message message;

    public MessageReader(Message message){
        this.message = message;
    }

    public void run(){
        Random random = new Random();
        for(String latestMessage = message.read();!latestMessage.equals("finished");latestMessage = message.read()){
            System.out.println(latestMessage);
            try{
                Thread.sleep(random.nextInt(3000));
            }
            catch(InterruptedException ex){

            }
        }
    }
}